package pl.cmitapp.clinicmanager.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import pl.cmitapp.clinicmanager.entity.CustomUserDetails;
import pl.cmitapp.clinicmanager.entity.User;
import pl.cmitapp.clinicmanager.service.UserService;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String theUsername) throws UsernameNotFoundException {
		User user = userService.getUserByUsername(theUsername);

		if (user == null) {
			throw new UsernameNotFoundException("Username not found");
		}
		
		
		return new CustomUserDetails(user);
	} 

}
