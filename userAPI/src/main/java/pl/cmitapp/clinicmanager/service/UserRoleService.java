package pl.cmitapp.clinicmanager.service;

import java.util.List;

import pl.cmitapp.clinicmanager.entity.UserRole;

public interface UserRoleService {
	
	List<UserRole> getAllroles();
	
	UserRole getById(int theId);
	
	UserRole getByRole(String theRole);

}
