package pl.cmitapp.clinicmanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import pl.cmitapp.clinicmanager.dao.UserRoleDAO;
import pl.cmitapp.clinicmanager.entity.UserRole;


public class UserRoleServiceImpl implements UserRoleService {
	
	@Autowired
	private UserRoleDAO userRoleDAO;

	@Override
	public List<UserRole> getAllroles() {
		
		return userRoleDAO.getAllroles();
	}

	@Override
	public UserRole getById(int theId) {
		
		return userRoleDAO.getById(theId);
	}

	@Override
	public UserRole getByRole(String theRole) {
		
		return userRoleDAO.getByRole(theRole);
	}

}
