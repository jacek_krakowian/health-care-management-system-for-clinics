package pl.cmitapp.clinicmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"pl.cmitapp.clinicmanager"})
public class ClinicManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClinicManagerApplication.class, args);
	}
}
