package pl.cmitapp.clinicmanager.dao;

import java.util.List;

import pl.cmitapp.clinicmanager.entity.UserRole;

public interface UserRoleDAO {
	
	List<UserRole> getAllroles();
	
	UserRole getById(int theId);
	
	UserRole getByRole(String theRole);

}
