package pl.cmitapp.clinicmanager.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import pl.cmitapp.clinicmanager.entity.User;
import pl.cmitapp.clinicmanager.service.UserService;

@RestController
@RequestMapping("/systems")
public class UserController {

	@Autowired
	private UserService userService;
	
	@GetMapping("/users")
	public ResponseEntity<List<User>> getAllUsers() {
		
		List<User> users = userService.getAllUsers();
		
		if(users.isEmpty()) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	@GetMapping("/users2")
	public ResponseEntity<List<User>> getUsersByRole(@RequestParam(value = "role", required = false) String theRole) {
		
		List<User> users = userService.getUsersByRole(theRole);
		
		if(users.isEmpty()) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	@GetMapping("/user/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") Integer theId) {
		
		User theUser = userService.getUserById(theId);
		
		if(theUser == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<User>(theUser, HttpStatus.OK);
	}
	
	//application/json; charset=UTF-8 - content-type must be set to work in postman
	@PostMapping("/user")
	public ResponseEntity<Void> addUser(@RequestBody User theUser, UriComponentsBuilder ucBuilder) {
		
		if(userService.isUserUnique(theUser) == false) {
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
        userService.addUser(theUser);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(theUser.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
	/*
	@PutMapping("/user")
	public ResponseEntity<User> updateUser(@RequestBody User theUser) {
		
		userService.updateUser(theUser);
		return new ResponseEntity<User>(theUser, HttpStatus.OK);
	}
	*/
	@PutMapping("/user/{id}")
	public ResponseEntity<User> updateUser(@PathVariable("id") Integer theId, @RequestBody User theUser) {
		
		User user = userService.getUserById(theId);
		
		if(user == null) {
			return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
		}
		userService.updateUser(theId, theUser);
		return new ResponseEntity<User>(theUser, HttpStatus.OK);
	}

	@DeleteMapping("/user/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable("id") int theId) {
		
		User user = userService.getUserById(theId);
		
		if(user == null) {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		userService.deleteUser(theId);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}	
	
	@DeleteMapping("/users")
	public ResponseEntity<User> deleteAllUsers() {
		
		userService.deleteAllUsers();
		return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<User>> searchUser(@RequestParam("q") String theQuery) {
		
		List<User> list = userService.searchUsers(theQuery);
		return new ResponseEntity<List<User>>(list, HttpStatus.OK);
		
	}
	
	@GetMapping("/search2")
	public ResponseEntity<List<User>> searchUserAdvanced(@RequestParam("q") String theQuery, 
			@RequestParam(value = "in", required = false) String filter ) {

		List<User> list = userService.searchUsersAdvanced(theQuery, filter);
		
		if(list.isEmpty()) {
			return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
		}
		
		return new ResponseEntity<List<User>>(list, HttpStatus.OK);
		
	}
	
}

