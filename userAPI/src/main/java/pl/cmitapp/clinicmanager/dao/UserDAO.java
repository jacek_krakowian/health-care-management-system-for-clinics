package pl.cmitapp.clinicmanager.dao;

import java.util.List;

import pl.cmitapp.clinicmanager.entity.User;

public interface UserDAO {
	
	public List<User> getAllUsers();

	public void addUser(User theUser);
	
	//public void updateUser(User theUser);
	
	public void updateUser(int theId, User theUser);

	public User getUserById(int theId);
	
	public User getUserByUsername(String theUsername);
	
	public User getUserByEmail(String theEmail);

	public void deleteUser(int theId);

	public List<User> searchUsers(String theQuery);
	
	public boolean isUserUnique(User theUser);
	
	public void deleteAllUsers();
	
	public List<User> getUsersByRole(String theRole);

	public List<User> searchUsersAdvanced(String theQuery, String filter);

}
