zeby uruchomic aplikacje potrzebny jest: 
0. zaimportowa� jako aplikacja maven
1. mySQL - https://dev.mysql.com/downloads/windows/installer/5.7.html
2. W mySQL Workbench trzeba stworzyc schema - uruchomic skrypt clinic-manager-db-new-v1.sql
3. W application.properties trzeba ustawic has�o(spring.datasource.password=) i u�ytkownika (spring.datasource.username=) na taki jaki jest ustawiony w mySQL wokrbench dla mySQL Connection (u�ytkownik bazowo nazywa si� root a has�o podajcie przy instalacji mySQL)
4. Do testowania API mo�na wykorzysta� dodatek do Chrome - Postman - https://www.google.pl/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=0ahUKEwjyi_Lk9aXaAhWCtBQKHcvfAaIQFggqMAA&url=https%3A%2F%2Fchrome.google.com%2Fwebstore%2Fdetail%2Fpostman%2Ffhbjgbiflinjbdggehcddcbncdddomop&usg=AOvVaw0bEk4JkKX1g2WxEBFiQDnf
5. Zeby uzyska� dost�p trzeba si� zalogowa� (w przpadku Postmana w "Authorization" ustawi�: "type" - Basic Auth, "Username" - admin, "Password" - admin (skrypt clinic-manager-db-new-v1.sql tworzy 1 u�ytkownika - admin o ha�le admin (zaszyfrowane BCRYPT)