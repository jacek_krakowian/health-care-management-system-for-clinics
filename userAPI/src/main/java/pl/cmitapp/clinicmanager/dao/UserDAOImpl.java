package pl.cmitapp.clinicmanager.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import pl.cmitapp.clinicmanager.entity.User;
import pl.cmitapp.clinicmanager.entity.User_;

@Repository
public class UserDAOImpl implements UserDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<User> getAllUsers() {

		String hql = "from User order by lastName";

		return entityManager.createQuery(hql, User.class).getResultList();
	}

	@Override
	public void addUser(User theUser) {

		entityManager.merge(theUser);
	}

	@Override
	public User getUserById(int theId) {

		return entityManager.find(User.class, theId);

	}

	@Override
	public User getUserByUsername(String theUsername) {

		Query query = null;
		User user = null;
		String hql = "from User where username like:username";

		query = entityManager.createQuery(hql, User.class);
		query.setParameter("username", theUsername);

		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {

		}
		return user;
	}

	@Override
	public User getUserByEmail(String theEmail) {

		Query query = null;
		User user = null;
		String hql = "from User where email like:email";

		query = entityManager.createQuery(hql, User.class);
		query.setParameter("email", theEmail);

		try {
			user = (User) query.getSingleResult();
		} catch (NoResultException e) {

		}
		return user;
	}

	@Override
	public void deleteUser(int theId) {
		// easy way but slow
		// entityManager.remove(getPatientById(theId));

		// faster
		String hql = "delete from User where id=:userId";
		Query query = entityManager.createQuery(hql);
		query.setParameter("userId", theId);
		query.executeUpdate();

	}

	@Override
	public List<User> searchUsers(String theQuery) {

		TypedQuery<User> query = null;
		String hql = "from User where lower(firstName) like:searchInput" + " or lower(lastName) like:searchInput"
				+ " or lower(username) like:searchInput or lower(email) like:searchInput";

		if (theQuery != null && theQuery.trim().length() > 0) {

			query = entityManager.createQuery(hql, User.class);
			query.setParameter("searchInput", "%" + theQuery.toLowerCase() + "%");

		} else {

			query = entityManager.createQuery("from User order by lastName", User.class);
		}

		return query.getResultList();
	}

	@Override
	public boolean isUserUnique(User theUser) {

		return (getUserByUsername(theUser.getUsername()) == null && getUserByEmail(theUser.getEmail()) == null);
	}
	
	/*DEPRECATED
	@Override
	public void updateUser(User theUser) {

		User user = getUserById(theUser.getId());
		user.setFirstName(theUser.getFirstName());
		user.setLastName(theUser.getLastName());
		user.setPhoneNumber(theUser.getPhoneNumber());
		entityManager.flush();
	}
	*/
	public void updateUser(int theId, User theUser) {

		User user = getUserById(theId);
		user.setFirstName(theUser.getFirstName());
		user.setLastName(theUser.getLastName());
		user.setPhoneNumber(theUser.getPhoneNumber());
		user.setBirthDate(theUser.getBirthDate());
		user.setEmail(theUser.getEmail());
		user.setGender(theUser.getGender());
		user.setUsername(theUser.getUsername());
		user.setPassword(theUser.getPassword());
		user.setRoles(theUser.getRoles());

		entityManager.merge(user);

	}

	@Override
	public void deleteAllUsers() {

		String hql = "delete from User";
		Query query = entityManager.createQuery(hql);
		query.executeUpdate();

	}
	@Override
	public List<User> getUsersByRole(String theRole) {
		
		TypedQuery<User> query = null;
		String hql;
		
		if(theRole == null) {
			hql = "from User order by lastName";
			query = entityManager.createQuery(hql, User.class);
		}else {
			hql = "select u from User u inner join u.userRoles ur where ur.role in :role";
			query = entityManager.createQuery(hql, User.class);
			query.setParameter("role",theRole);
		}
		return query.getResultList();
	}

	@Override
	public List<User> searchUsersAdvanced(String theQuery, String filter) {
		
		// QUERY BY USING CRITERIA BULIDER 
		/*
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> q = cb.createQuery(User.class);
		Root<User> u = q.from(User.class);
		q.select(u);
		ParameterExpression<String> param = cb.parameter(String.class);
		if (filter == null) {
			q.where(cb.or(
						
					cb.like(cb.lower(u.get("firstName")), param),
					cb.like(cb.lower(u.get("lastName")), param),
					cb.like(cb.lower(u.get("username")), param),
					cb.like(cb.lower(u.get("email")), param)

			));
		}else {
			q.where(cb.like(u.get(filter), param));
		}
			
		TypedQuery<User> query = entityManager.createQuery(q);
		theQuery.trim().length();
		query.setParameter(param, "%" + theQuery.toLowerCase() + "%");
		List<User> results = query.getResultList();

		return results;
		*/
		//STANDARD JPQL
		TypedQuery<User> query = null;
		String hql;
		theQuery.trim().length();
		
		if(filter == null) {
			hql = "from User where lower(firstName) like:searchInput" + " or lower(lastName) like:searchInput"
				+ " or lower(username) like:searchInput or lower(email) like:searchInput";
			query = entityManager.createQuery(hql, User.class);
		}else {
			hql = "from User where lower(" + filter + ") like:searchInput";
			
		}
		
		query = entityManager.createQuery(hql, User.class);
		query.setParameter("searchInput", "%" + theQuery.toLowerCase() + "%");
		return query.getResultList();
		
		
		//TO IMPLEMENT DYNAMIC SEARCH - multiple filters 
		/*
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<User> criteria = builder.createQuery(User.class);
		Root<User> pRoot = criteria.from(User.class);
		//Join<Person, Language> langJoin = criteria.join("language", JoinType.LEFT);
		ParameterExpression<String> param = builder.parameter(String.class);
		
		List<Predicate> restrictions = new ArrayList<Predicate>();
		restrictions.add(builder.like(builder.lower(pRoot.get(User_.firstName)), param));
		restrictions.add(builder.like(builder.lower(pRoot.get(User_.lastName)), param));
		restrictions.add(builder.like(builder.lower(pRoot.get(User_.username)), param));
		restrictions.add(builder.like(builder.lower(pRoot.get(User_.email)), param));
		
		criteria.where(builder.or(restrictions.toArray(new Predicate[restrictions.size()])));
		criteria.orderBy(builder.asc(pRoot.get(User_.firstName)));
		
		TypedQuery<User> query = entityManager.createQuery(criteria);
		theQuery.trim().length();
		query.setParameter(param, "%" + theQuery.toLowerCase() + "%");
		List<User> results = query.getResultList();

		return results;
		*/
	}
		

}
