package pl.cmitapp.clinicmanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.cmitapp.clinicmanager.dao.UserDAO;
import pl.cmitapp.clinicmanager.entity.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	@Override
	public List<User> getAllUsers() {
		
		return userDAO.getAllUsers();
	}

	@Override
	public void addUser(User theUser) {
		
		userDAO.addUser(theUser);
		
	}

	@Override
	public User getUserById(int theId) {
		
		return userDAO.getUserById(theId);
	}
	
	@Override
	public User getUserByUsername(String theUsername) {

		return userDAO.getUserByUsername(theUsername);
	}

	@Override
	public void deleteUser(int theId) {
		
		userDAO.deleteUser(theId);
		
	}

	@Override
	public List<User> searchUsers(String theQuery) {
		
		return userDAO.searchUsers(theQuery);
	}

	@Override
	public boolean isUserUnique(User theUser) {
		
		return userDAO.isUserUnique(theUser);
	}
	/*
	@Override
	public void updateUser(User theUser) {
		
		userDAO.updateUser(theUser);
	}
	*/
	@Override
	public void updateUser(int theId, User theUser) {
		
		userDAO.updateUser(theId, theUser);
		
	}

	@Override
	public void deleteAllUsers() {
		
		userDAO.deleteAllUsers();
	}

	@Override
	public User getUserByEmail(String theEmail) {
		
		return userDAO.getUserByEmail(theEmail);
	}

	@Override
	public List<User> getUsersByRole(String theRole) {
		
		return userDAO.getUsersByRole(theRole);
	}

	@Override
	public List<User> searchUsersAdvanced(String theQuery, String filter) {
		
		return userDAO.searchUsersAdvanced(theQuery, filter);
	}

	

}