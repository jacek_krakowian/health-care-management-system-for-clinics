package pl.cmitapp.clinicmanager.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import pl.cmitapp.clinicmanager.entity.UserRole;

public class UserRoleDAOImpl implements UserRoleDAO {
	
	@PersistenceContext	
	private EntityManager entityManager;	

	@Override
	public List<UserRole> getAllroles() {
		
		String hql = "from UserRole order by role";
		
		return entityManager.createQuery(hql, UserRole.class).getResultList();
	}

	@Override
	public UserRole getById(int theId) {
		
		return entityManager.find(UserRole.class, theId);
	}

	@Override
	public UserRole getByRole(String theRole) {
		
		Query query = null;
		String hql = "from UserRole where role like:role";
		
		query = entityManager.createQuery(hql, UserRole.class);
		query.setParameter("role", theRole);
		
		return (UserRole) query.getSingleResult();
	}

	
	
}
