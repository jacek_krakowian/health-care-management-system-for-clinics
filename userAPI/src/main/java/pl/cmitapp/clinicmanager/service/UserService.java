package pl.cmitapp.clinicmanager.service;

import java.util.List;

import pl.cmitapp.clinicmanager.entity.User;

public interface UserService {

	public List<User> getAllUsers();

	public void addUser(User theUser);
	
	//public void updateUser(User theUser);
	
	public void updateUser(int theId, User theUser);

	public User getUserById(int theId);
	
	public User getUserByUsername(String theUsername);
	
	public User getUserByEmail(String theEmail);

	public void deleteUser(int theId);
	
	public void deleteAllUsers();

	public List<User> searchUsers(String theQuery);
	
	public List<User> searchUsersAdvanced(String theQuery, String filter);
	
	public boolean isUserUnique(User theUser);
	
	public List<User> getUsersByRole(String theRole);

	
}
